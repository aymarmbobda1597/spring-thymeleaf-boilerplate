package com.maitech.demo.security;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.maitech.demo.exceptions.BadRequestException;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationmanager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationmanager) {
        super();
        this.authenticationmanager = authenticationmanager;
        setFilterProcessesUrl(SecurityConstant.AUTH_LOGIN_URL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        JsonObject userJsonObject;
        String login;
        String password;

        try {
            Scanner scanner = new Scanner(request.getInputStream()).useDelimiter("\\A");
            String userJson = scanner.hasNext() ? scanner.next() : "";
            userJsonObject = new Gson().fromJson(userJson, JsonObject.class);

            login = userJsonObject.get("username").getAsString();
            password = userJsonObject.get("password").getAsString();
            return authenticationmanager.authenticate(new UsernamePasswordAuthenticationToken(login, password));
        } catch (IOException ioException) {
            throw new BadRequestException(ioException.getMessage());
        }

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) {
        CustomUserDetails springUser = (CustomUserDetails) authResult.getPrincipal();

        String jwt = Jwts.builder().setSubject(springUser.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstant.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SecurityConstant.SECRET)
                .claim("role", springUser.getRole())
                .compact();

        response.addHeader(SecurityConstant.HEADER_STRING, jwt);
        //response.addHeader(SecurityConstant.HEADER_STRING, SecurityConstant.TOKEN_PREFIX + jwt);
    }
}