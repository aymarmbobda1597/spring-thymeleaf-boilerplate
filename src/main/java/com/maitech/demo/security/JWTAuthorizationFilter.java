package com.maitech.demo.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterchain) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "*");
        response.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, " + "Access-Control-Request-Method, " + "Access-Control-Request-Headers, " + "Authorization");
        response.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin," + "Access-Control-Allow-Credentials, Authorization");

        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            String jwt = request.getHeader(SecurityConstant.HEADER_STRING);
            if (jwt == null || !jwt.startsWith(SecurityConstant.TOKEN_PREFIX)) {
                try {
                    filterchain.doFilter(request, response);
                } catch (IOException | ServletException e) {
                    e.printStackTrace();
                }

                return;
            }

            Claims claim = Jwts.parser().setSigningKey(SecurityConstant.SECRET).parseClaimsJws(jwt.replace(SecurityConstant.TOKEN_PREFIX, "")).getBody();
            String login = claim.getSubject();
            String role = (String) claim.get("role");

            List<SimpleGrantedAuthority> userAuthorities = new ArrayList<>();
            userAuthorities.add(new SimpleGrantedAuthority(role));

            UsernamePasswordAuthenticationToken authenticatedUser = new UsernamePasswordAuthenticationToken(login, null, userAuthorities);
            SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
            
            try {
                filterchain.doFilter(request, response);
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
        }
    }
}