package com.maitech.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

    @Order(1)
    @Configuration
    public static class APISecurityConfigAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        private BCryptPasswordEncoder bCryptPasswordEncoder;

        @Autowired
        private UserDetailsServiceImpl userDetailsService;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable();

            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

            http.authorizeRequests().antMatchers("/api/v1/**").permitAll();

            http.authorizeRequests().antMatchers("/api/v1/roles/**", "/api/v1/users/**").hasAnyRole("USER", "ADMIN");

            http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/v1/roles/**").hasAnyRole("ADMIN");

            http.authorizeRequests().anyRequest().authenticated();

            http.addFilter(new JWTAuthenticationFilter(authenticationManager()));

            http.addFilterBefore(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
        }
    }

    @Order(2)
    @Configuration
    public static class webSecurityConfigAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        private BCryptPasswordEncoder bCryptPasswordEncoder;

        @Autowired
        private UserDetailsServiceImpl userDetailsService;
        
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
        }
        
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.cors().and().csrf().disable();

            http.authorizeRequests().antMatchers("/", "/login", "/signIn", "/signUp").permitAll();

            http.authorizeRequests().antMatchers("/app/**").hasAnyAuthority("USER", "ADMIN");

            http.authorizeRequests().anyRequest().authenticated();

            http.formLogin().loginPage(SecurityConstant.AUTH_SIGNIN_URL).loginProcessingUrl("/signin")
                    .defaultSuccessUrl(SecurityConstant.AUTH_SIGNIN_SUCCESS_URL).permitAll()
                    .usernameParameter("email").passwordParameter("password");
            
            http.logout().invalidateHttpSession(true).clearAuthentication(true)
                    .logoutRequestMatcher(new AntPathRequestMatcher(SecurityConstant.AUTH_SIGNOUT_URL))
                    .logoutSuccessUrl("/").deleteCookies("JSESSIONID");
            
            http.rememberMe().tokenValiditySeconds(SecurityConstant.REMEBER_TIME).key(SecurityConstant.REMEBER_SECRET)
                    .rememberMeServices(null).rememberMeParameter("rememnerMe").useSecureCookie(true);
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                    "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**", "/images/**", "/scss/**",
                    "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png", "/v2/api-docs", "/configuration/ui",
                    "/configuration/security", "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                    "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/");
        }
    }
}
