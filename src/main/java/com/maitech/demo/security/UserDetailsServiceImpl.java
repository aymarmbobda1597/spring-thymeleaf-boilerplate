package com.maitech.demo.security;

import com.maitech.demo.models.entities.UserEntity;
import com.maitech.demo.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserEntity user =  null;

        if (login.contains("@")) {
            user = userRepository.findByEmail(login);
        }
        else {
            user = userRepository.findByPhone(login);
        }

        if (user == null)
            throw new UsernameNotFoundException(login);

        String subject = user.getEmail();
        String password = user.getPassword();
        String role = user.getRoleEntity().getName();

        return new CustomUserDetails(subject, password, role);
    }
}