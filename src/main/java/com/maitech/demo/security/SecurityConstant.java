package com.maitech.demo.security;

public class SecurityConstant {
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTH_LOGIN_URL = "/login";
    public static final String HEADER_STRING = "Authorization";
    public static final long EXPIRATION_TIME = 1 * 24 * 3600 * 1000; // days * hours/days * seconds/hours * milliseconds/second
    public static final String SECRET = "$2a$10$/GsdVwwv3ZDhzKRcO3WyX.k6v649tZwkc5UAjdo9meYCrz5lpHUii";

    public static final String AUTH_SIGNIN_URL = "/signIn";
    public static final String AUTH_SIGNOUT_URL = "/signOut";
    public static final String AUTH_SIGNIN_SUCCESS_URL = "/account/home";
    public static final String AUTH_SIGNIN_ERROR_URL = "/signIn?error=true";
    public static final int REMEBER_TIME = 1 * 24 * 60 * 60; // days * hours/day * minutes/hours * seconds/minutes
    public static final String REMEBER_SECRET = "$2a$10$pf041CtQryOThLzNI2tp0ORuqFvPvv//N0SBFB5QixD5tSk/.LNMG";
}
