package com.maitech.demo.repositories;

import com.maitech.demo.models.entities.UserEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    UserEntity findByEmail(String email);

    UserEntity findByPhone(String phone);

    @Query(value = "SELECT * FROM users_tables WHERE ((firstName like %keyword%) OR (lastname like %keyword%) OR (email like %keyword%) OR (gender like %keyword%) OR (lastname like %keyword%))", nativeQuery = true)
    Page<UserEntity> searchByKeword(Pageable pageable, String keyword);
}
