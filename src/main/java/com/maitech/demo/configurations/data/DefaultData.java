package com.maitech.demo.configurations.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class DefaultData {
    private static final Logger logger = LoggerFactory.getLogger(DefaultData.class);
    
    private void loadData() {
    }
    
    @Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

    @Bean
    public CommandLineRunner demo() {
        return (args) -> {
            logger.info("Load of default datas");
            loadData();
        };
    }
}
