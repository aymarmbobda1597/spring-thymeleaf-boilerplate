package com.maitech.demo.configurations.openapi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class OpenAPICOnfiguration {
    /**
     * http://localhost:{port}/v3/api-docs
     * http://localhost:{port}/v3/api-docs.yaml
     * http://localhost:{port}/swagger-ui.html
     */

    private Info info() {
        Contact contact = new Contact();
        contact.setName("");
        contact.setEmail("");
        contact.setUrl("");

        License licence = new License();
        licence.name("Apache 2.0");
        licence.url("http://springdoc.org");

        return new Info().title("JWT API").description("").version("0.1").contact(contact).license(licence);
    }

    @Bean
    public OpenAPI customOpenAPI() {
        ExternalDocumentation externalDocs = new ExternalDocumentation();
        externalDocs.description("");
        externalDocs.url("");

        return new OpenAPI().components(new Components()).info(info()).externalDocs(externalDocs);
    }
}