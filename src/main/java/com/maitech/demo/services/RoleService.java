package com.maitech.demo.services;

import com.maitech.demo.models.entities.RoleEntity;
import com.maitech.demo.repositories.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleService {
    
    @Autowired
    private RoleRepository roleRepository;

    public Page<RoleEntity> findRoles(Pageable pageable) {
        return roleRepository.findAll(pageable);
    }

    public RoleEntity findRoleById(String id) {
        return roleRepository.findById(id).orElse(null);
    }

    public RoleEntity saveRole(RoleEntity roleEntity) {
        RoleEntity role = new RoleEntity();

        if (roleEntity.getId() != null)
            role.setId(roleEntity.getId());

        role.setName(roleEntity.getName());
        role.setDescription(roleEntity.getDescription());

        return roleRepository.save(role);
    }

    public boolean deleteRoleById(String id) {
        RoleEntity role = roleRepository.findById(id).orElse(null);

        if (role == null)
            return false;
        
        roleRepository.delete(role);

        return true;
    }
}
