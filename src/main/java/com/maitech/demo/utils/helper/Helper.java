package com.maitech.demo.utils.helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class Helper {
    private String DIR_TO_UPLOAD = "/uploads"; // "C:\\Users\\Atul\\Desktop\\sparrow\\"

    private static final Logger logger = LoggerFactory.getLogger(Helper.class);
    
    public byte[] compressByte(byte[] datas) {
        byte[] buffer = new byte[1024];
        Deflater deflater = new Deflater();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(datas.length);

        deflater.setInput(datas);
        deflater.finish();

        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }

        try {
            outputStream.close();
        } 
        catch (IOException ioException) {
            logger.error(ioException.getMessage());
        }
        String text = "Compressed Data Byte Size - " + outputStream.toByteArray().length;
        logger.info(text);
        return outputStream.toByteArray();
    }

    public byte[] decompressBytes(byte[] data) {
        byte[] buffer = new byte[1024];
        Inflater inflater = new Inflater();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

        inflater.setInput(data);

        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } 
        catch (IOException ioException) {
            logger.error(ioException.getMessage());
        }
        catch (DataFormatException dataFormatException) {
            logger.error(dataFormatException.getMessage());
        }

        return outputStream.toByteArray();
    }

    /*public boolean uploadImageInBase64(String base64Image, String fileName) throws Exception {
        if (Base.isBase64(base64Image) == false) {
            return false;
        }

        byte[] imageByte = Base64.decodeBase64(base64Image);
        String directory = servletContext.getRealPath("/") + "images/" + fileName;
        new FileOutputStream(directory).write(imageByte);
    }*/

    public void uploadImage(MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        Path path = Paths.get(DIR_TO_UPLOAD + file.getOriginalFilename());
        Files.write(path, bytes);
    }
}