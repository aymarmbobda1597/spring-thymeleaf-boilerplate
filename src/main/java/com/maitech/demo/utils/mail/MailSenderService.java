package com.maitech.demo.utils.mail;

import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class MailSenderService {/*

    @Autowired
    private JavaMailSender emailSender;

    public void sendEmail(String mail_receiver, String mail_subject, String mail_content) {

        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(mail_receiver);
        //message.setTo("mail_receiver1", "mail_receiver2");

        message.setSubject(mail_subject);

        message.setText(mail_content);

        emailSender.send(message);
    }

    public void sendEmailWithAttachment(String mail_receiver, String mail_subject, String mail_content, String file_description, String pathToAttachment) throws MessagingException, IOException {

        MimeMessage message = emailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        
        helper.setTo(mail_receiver);

        helper.setSubject(mail_subject);

        // default = text/plain
        helper.setText(mail_content);

        FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
        helper.addAttachment(file_description, file);

        emailSender.send(message);
    }

    public void sendEmailWithLocalAttachment(String mail_receiver, String mail_subject, String mail_content, String file_description, String pathToAttachment) throws MessagingException, IOException {

        MimeMessage message = emailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        
        helper.setTo(mail_receiver);

        helper.setSubject(mail_subject);

        // default = text/plain
        helper.setText(mail_content);

        helper.addAttachment(file_description, new ClassPathResource(pathToAttachment));

        emailSender.send(message);
    } */
}
