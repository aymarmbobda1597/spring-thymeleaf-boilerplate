package com.maitech.demo.exceptions;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse {

    private Date timestamp;

    private String message;

    private String details;

    private Throwable cause;
}