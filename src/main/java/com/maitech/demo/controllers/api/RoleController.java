package com.maitech.demo.controllers.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

//import javax.validation.Valid;

import com.maitech.demo.exceptions.BadRequestException;
import com.maitech.demo.exceptions.NotFoundException;
import com.maitech.demo.models.dtos.RoleDto;
import com.maitech.demo.models.entities.RoleEntity;
import com.maitech.demo.services.RoleService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/roles")
@Tag(name = "Role", description = "The role API ")
public class RoleController {
    private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private RoleService roleService;
    
    /**
     * @description Find All Roles
     * @param pageable
     * @return
     */
    @GetMapping(value = "", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    @Operation(summary = "Find All Roles", description = "Return a role list", tags = {"role"})
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful operation", content = @Content(array = @ArraySchema(schema = @Schema(implementation = RoleDto.class))))})
    private List<RoleDto> findRoles(Pageable pageable) {
        logger.info("Find All Roles");
        List<RoleDto> roleDtosList = new ArrayList<>(); 
        Page<RoleEntity> rolePage = this.roleService.findRoles(pageable);
        
        rolePage.getContent().forEach(roleEntity -> roleDtosList.add(new RoleDto(roleEntity)));
        
        return roleDtosList;
    }
    
    /**
     * @description Find One Role By Id
     * @param id
     * @return
     */
    @Operation(summary = "Get a role by its id")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Found the role", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = RoleDto.class))}), @ApiResponse(responseCode = "404", description = "Invalid id supplied", content = @Content), @ApiResponse(responseCode = "404", description = "Role not found", content = @Content)})
    @GetMapping(value = "{id}", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public RoleDto findRoleById(@PathVariable String id) {
        logger.info("Find Role By Id.");
        RoleEntity roleEntity = this.roleService.findRoleById(id);

        if (roleEntity == null) {
            logger.error("Role Not Found.");
            throw new NotFoundException("Role Not found!");
        }

        return new RoleDto(roleEntity);
    }

    /**
     * @description Save One Role
     * @param roleDto
     * @return
     */
    @PostMapping(value = "", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    @Operation(summary = "Create New or Update role", description = "Returns a single role", tags = {"role" })
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Role saved", content = @Content(schema = @Schema(implementation = RoleDto.class))), @ApiResponse(responseCode = "400", description = "Invalid Input")})
    public RoleDto saveRole(@Valid @RequestBody RoleDto roleDto) {
        logger.info("Save One Role.");
        return new RoleDto(this.roleService.saveRole(roleDto.toRoleEntity()));
    }

    /**
     * @description Delete One Role By Id
     * @param id
     * @return
     */
    @DeleteMapping(value = "{id}",  consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    @Operation(summary = "Deletes a role", description = "Return a string", tags = {"role"})
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Successful operation"), @ApiResponse(responseCode = "404", description = "Role not found")})
    public String deleteRoleById(@PathVariable String id) {
        logger.info("Delete Role By Id.");
        boolean isDeleted = this.roleService.deleteRoleById(id);

        if (!isDeleted) {
            logger.error("Role Not Deleted Because Not Found.");
            throw new BadRequestException("Role not deleted because it Not found!");
        }

        return "Role deleted successfully!";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER')")
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public String defaultPage() {
		return "You have USER role.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
    //@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public String getAllBlogs() {
		return "You have ADMIN role.";
	}
}