package com.maitech.demo.models.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "users_tables")
public class UserEntity {
    
    @Id
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private String id;

    @Column(name = "lastname", nullable = false, length = 50)
    private String lastname;

    @Column(name = "firstname", nullable = false, length = 50)
    private String firstname;

    @Column(name = "gender", nullable = false, length = 50)
    private String gender;

    @Column(name = "phone", unique = true, nullable = false, length = 50)
    private String phone;

    @Column(name = "email", unique = true, nullable = false, length = 50)
    private String email;

    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @Transient
    private String repeat_password;

    @ManyToOne(fetch = FetchType.LAZY)
    private RoleEntity roleEntity;
    
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    //@DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date created_at;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    //@DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date updated_at;
    
    public UserEntity(String email) {
        this.email = email;
    }
}
