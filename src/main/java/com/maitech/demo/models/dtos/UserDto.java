package com.maitech.demo.models.dtos;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import com.maitech.demo.models.entities.UserEntity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String id;

    @NotEmpty
    private String firstname;

    @NotEmpty
    private String lastname;

    @NotEmpty
    @Size(min = 1, max = 8)
    private String gender;

    @NotEmpty(message = "Please provide a Phone")
    @Pattern(regexp = "^(\\+\\d{1,3}( )?)?((\\(\\d{1,3}\\))|\\d{1,3})[- .]?\\d{3,4}[- .]?\\d{4}$", message = "Phone not correct")
    private String phone;

    @Email(message = "Invalide Email")
    @NotEmpty(message = "Please provide a Email")
    private String email;

    @JsonProperty(access = Access.WRITE_ONLY)
    @Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W_]).{6,50})", message = "Password must contain minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character")
    private String password;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String repeat_password;

    @JsonProperty(access = Access.READ_ONLY)
    private String created_at;

    @JsonProperty(access = Access.READ_ONLY)
    private String updated_at;

    public UserDto(UserEntity userEntity) {
        this.id = userEntity.getId();
        this.lastname = userEntity.getLastname();
        this.firstname = userEntity.getFirstname();
        this.gender = userEntity.getGender();
        this.phone = userEntity.getPhone();
        this.email = userEntity.getEmail();
        this.password = userEntity.getPassword();
        this.created_at = userEntity.getCreated_at().toString();
        this.updated_at = userEntity.getUpdated_at().toString();
    }

    public UserEntity toCreateUser() {
        return null;
    }
    
    public UserEntity toUpdateUser() {  
        return null;     
    }
}