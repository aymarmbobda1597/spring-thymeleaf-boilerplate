package com.maitech.demo.models.dtos;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.maitech.demo.models.entities.RoleEntity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RoleDto implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String id;

    @NotEmpty
    @Size(min = 2, max = 25, message = "")
    private String name;

    @NotEmpty
    @Size(min = 2, max = 8, message = "")
    private String description;

    public RoleDto(RoleEntity roleEntity) {
        this.id = roleEntity.getId();
        this.name = roleEntity.getName();
        this.description = roleEntity.getDescription();
    }

    public RoleEntity toRoleEntity() {
        RoleEntity roleEntity = new RoleEntity();

        roleEntity.setId(id);
        roleEntity.setName(name);
        roleEntity.setDescription(description);

        return roleEntity;
    }
}
